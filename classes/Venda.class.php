<?php
require_once "autoload.php";

class Venda extends Produto
{
    private $iIdVenda, $iQuantidade;

    /*
     * Retorna o ID de venda
     *
     * @return integer
     */
    public function getIdVenda() {
      return $this->iIdVenda;
    }

    /*
     * Seta o ID de venda
     *
     * @param integer
     *
     * @return void
     */
    public function setIdVenda($iIdVenda) {
      $this->iIdVenda = $iIdVenda;
    }

    /*
     * Retorna a quantidade de venda
     *
     * @return integer
     *
     */
    public function getQuantidade() {
      return $this->quantidade;
    }

    /*
     * Seta a quantidade de venda
     *
     * @param integer
     *
     * @return integer
     *
     */
    public function setQuantidade($iQuantidade) {
      $this->iQuantidade = $iQuantidade;
    }

    /*
     * Insere valores na tabela vendas
     *
     * @return integer
     *
     */
    public function insertVenda() {
      try {
          $banco = Conexao::getInstance();
          $pdo = $banco->getConexao();
          $stmt = $pdo->prepare('INSERT INTO VENDA (QUANTIDADE, PRODUTO_IDPRODUTO) VALUES( :QUANTIDADE, :IDPRODUTO)');
          $iIdProduto= parent::getIdProduto();
          $stmt->bindParam(':QUANTIDADE', $this->iQuantidade);
          $stmt->bindParam(':IDPRODUTO', $iIdProduto);
          $stmt->execute();
          if ($stmt->rowCount() == 0) {
              var_dump($stmt->errorInfo());
          } else {
              return true;
          }
      } catch (PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    /*
     * Faz a listagem da tabela vendas-
     *
     * @return integer
     *
     */
    public function listaVenda() {
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('
              SELECT V.QUANTIDADE
                  , P.DESCRICAO
                  , P.VALORUNITARIO
                  , P.VALORTOTALVENDAS
                FROM VENDA V
          INNER JOIN PRODUTO P
                  ON V.PRODUTO_IDPRODUTO = P.IDPRODUTO
          ');
        $stmt->execute();
        $aProduto = $stmt->fetchAll();
        return $aProduto;
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    /*
     * Busca o total de vendas
     *
     * @return integer
     *
     */
    public function selectQtdVenda() {
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('SELECT COUNT(*) AS "TOTALV" FROM VENDA');
        $stmt->execute();
        $aProduto = $stmt->fetchAll();
        if ($stmt->rowCount() == 0) {
          return '0';
        } else {
          return $aProduto[0]['TOTALV'];
        }
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }
}
