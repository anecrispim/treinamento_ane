<?php
require_once "autoload.php";

class Produto
{
    private $iIdProduto, $sDescricao, $nValorUnitario, $iEstoque, $iCodigoBarra, $bDeletado, $sDataUltimaVenda, $iValorTotalVendas;

    /*
     * Seta o ID do Produto
     *
     * @return integer
     */
    public function getIdProduto() {
      return $this->iIdProduto;
    }

    public function setIdProduto($iIdProduto) {
      $this->iIdProduto = $iIdProduto;
    }

    public function getDescricao() {
      return $this->sDescricao;
    }

    public function setDescricao($sDescricao) {
      $this->sDescricao = $sDescricao;
    }

    public function getValorUnitario() {
      return $this->nValorUnitario;
    }

    public function setValorUnitario($nValorUnitario) {
      $this->nValorUnitario = $nValorUnitario;
    }

    public function getEstoque() {
      return $this->iEstoque;
    }

    public function setEstoque($iEstoque) {
      $this->iEstoque = $iEstoque;
    }

    public function getCodigoBarra() {
      return $this->iCodigoBarra;
    }

    public function setCodigoBarra($iCodigoBarra) {
      $this->iCodigoBarra = $iCodigoBarra;
    }

    public function getDeletado() {
      return $this->bDeletado;
    }

    public function setDeletado($bDeletado) {
      $this->bDeletado = $bDeletado;
    }

    public function getDataUltimaVenda() {
      return $this->sDataUltimaVenda;
    }

    public function setDataUltimaVenda($sDataUltimaVenda) {
      $this->sDataUltimaVenda = $sDataUltimaVenda;
    }

    public function getValorTotalVendas() {
      return $this->iValorTotalVendas;
    }

    public function setValorTotalVendas($iValorTotalVendas) {
      $this->iValorTotalVendas = $iValorTotalVendas;
    }

    public function insertProduto() {
      try {
          $banco = Conexao::getInstance();
          $pdo = $banco->getConexao();
          $stmt = $pdo->prepare('INSERT INTO PRODUTO (DESCRICAO, VALORUNITARIO, ESTOQUE, CODIGOBARRA, DELETADO) VALUES( :DESCRICAO, :VALORUNITARIO, :ESTOQUE, :CODIGOBARRA, :DELETADO)');
          $stmt->bindParam(':DESCRICAO', $this->sDescricao);
          $stmt->bindParam(':VALORUNITARIO', $this->nValorUnitario);
          $stmt->bindParam(':ESTOQUE', $this->iEstoque);
          $stmt->bindParam(':CODIGOBARRA', $this->iCodigoBarra);
          $stmt->bindParam(':DELETADO', $this->bDeletado);
          $stmt->execute();
          if ($stmt->rowCount() == 0) {
              var_dump($stmt->errorInfo());
          } else {
              return true;
          }
      } catch (PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

  public function updateProduto() {
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('UPDATE PRODUTO SET DESCRICAO = :DESCRICAO, VALORUNITARIO = :VALORUNITARIO, ESTOQUE = :ESTOQUE, CODIGOBARRA = :CODIGOBARRA, DELETADO = :DELETADO WHERE IDPRODUTO = :IDPRODUTO');
        $stmt->bindParam(':DESCRICAO', $this->sDescricao);
        $stmt->bindParam(':VALORUNITARIO', $this->nValorUnitario);
        $stmt->bindParam(':ESTOQUE', $this->iEstoque);
        $stmt->bindParam(':CODIGOBARRA', $this->iCodigoBarra);
        $stmt->bindParam(':DELETADO', $this->bDeletado);
        $stmt->bindParam(':IDPRODUTO', $this->iIdProduto);
        $stmt->execute();
        if ($stmt->rowCount() == 0) {
          var_dump($stmt->errorInfo());
        }else{
          return true;
        }
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function listaProduto() {
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('SELECT * FROM PRODUTO WHERE DELETADO = FALSE');
        $stmt->execute();
        $aProduto = $stmt->fetchAll();
        return $aProduto;
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function updateProdutoLixeira(){
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('UPDATE PRODUTO SET DELETADO = :DELETADO WHERE IDPRODUTO = :IDPRODUTO');
        $stmt->bindParam(':DELETADO', $this->bDeletado);
        $stmt->bindParam(':IDPRODUTO', $this->iIdProduto);
        $stmt->execute();
        if ($stmt->rowCount() == 0) {
          var_dump($stmt->errorInfo());
        }else{
          return true;
        }
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function listaProdutoLixeira(){
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('
               SELECT *
                 FROM PRODUTO
                WHERE DELETADO = TRUE');
        $stmt->execute();
        $aProduto = $stmt->fetchAll();
        return $aProduto;
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function selectValorUnitario() {
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('SELECT VALORUNITARIO FROM PRODUTO WHERE IDPRODUTO = :IDPRODUTO');
        $stmt->bindParam(':IDPRODUTO', $this->iIdProduto);
        $stmt->execute();
        $aProduto = $stmt->fetchAll();
        return $aProduto[0]['VALORUNITARIO'];
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function updateProdutoVendaSemValorUnitario() {
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('UPDATE PRODUTO SET VALORTOTALVENDAS = :VALORTOTALVENDAS, DATAULTIMAVENDA = :DATAULTIMAVENDA WHERE IDPRODUTO = :IDPRODUTO');
        $stmt->bindParam(':VALORTOTALVENDAS', $this->iValorTotalVendas);
        $stmt->bindParam(':DATAULTIMAVENDA', $this->sDataUltimaVenda);
        $stmt->bindParam(':IDPRODUTO', $this->iIdProduto);
        $stmt->execute();
        if ($stmt->rowCount() == 0) {
          var_dump($stmt->errorInfo());
        }else{
          return true;
        }
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function updateProdutoVendaComValorUnitario(){
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('UPDATE PRODUTO SET VALORUNITARIO = :VALORUNITARIO, VALORTOTALVENDAS = :VALORTOTALVENDAS, DATAULTIMAVENDA = :DATAULTIMAVENDA WHERE IDPRODUTO = :IDPRODUTO');
        $stmt->bindParam(':VALORUNITARIO', $this->nValorUnitario);
        $stmt->bindParam(':VALORTOTALVENDAS', $this->iValorTotalVendas);
        $stmt->bindParam(':DATAULTIMAVENDA', $this->sDataUltimaVenda);
        $stmt->bindParam(':IDPRODUTO', $this->iIdProduto);
        $stmt->execute();
        if ($stmt->rowCount() == 0) {
          var_dump($stmt->errorInfo());
        }else{
          return true;
        }
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }

    public function selectQtdProduto(){
      try {
        $banco= Conexao::getInstance();
        $pdo= $banco->getConexao();
        $stmt = $pdo->prepare('SELECT COUNT(*) AS "TOTALP" FROM PRODUTO');
        $stmt->execute();
        $aProduto = $stmt->fetchAll();
        if ($stmt->rowCount() == 0) {
          return '0';
        } else {
          return $aProduto[0]['TOTALP'];
        }
        } catch(PDOException $e) {
          return 'Error: ' . $e->getMessage();
      }
    }
}
