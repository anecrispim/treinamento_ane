-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema AvaliacaoAC
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema AvaliacaoAC
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `AvaliacaoAC` DEFAULT CHARACTER SET utf8 ;
USE `AvaliacaoAC` ;

-- -----------------------------------------------------
-- Table `AvaliacaoAC`.`Produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AvaliacaoAC`.`PRODUTO` (
  `IDPRODUTO` INT NOT NULL AUTO_INCREMENT,
  `DESCRICAO` VARCHAR(45) NULL,
  `VALORUNITARIO` DECIMAL NULL,
  `ESTOQUE` INT NULL,
  `CODIGOBARRA` INT NULL,
  `DELETADO` TINYINT NULL,
  `DATAULTIMAVENDA` DATE NULL,
  `VALORTOTALVENDAS` DECIMAL NULL,
  PRIMARY KEY (`IDPRODUTO`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AvaliacaoAC`.`Venda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AvaliacaoAC`.`VENDA` (
  `IDVENDA` INT NOT NULL AUTO_INCREMENT,
  `QUANTIDADE` INT NULL,
  `PRODUTO_IDPRODUTO` INT NOT NULL,
  PRIMARY KEY (`IDVENDA`),
  INDEX `fk_VENDA_PRODUTO_idx` (`PRODUTO_IDPRODUTO` ASC),
  CONSTRAINT `fk_VENDA_PRODUTO`
    FOREIGN KEY (`PRODUTO_IDPRODUTO`)
    REFERENCES `AvaliacaoAC`.`PRODUTO` (`IDPRODUTO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
